﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class PlayerController : MonoBehaviour { 
	
	
	public float velocidadMovimiento;
	public float velocidadRotacion;

    public GameObject Activar;
    bool play = false;

	void Update ()  
	{
        if (play)
        {
            this.transform.position += this.transform.forward * velocidadMovimiento * Time.deltaTime * Input.GetAxis("Vertical");
            this.transform.Rotate(0, velocidadRotacion * Input.GetAxis("Horizontal") * Time.deltaTime, 0, Space.Self);
        }
           
       		
	}


    void OnMouseDown()
    {
        play = true;
        Debug.Log("click inicio");
        Activar.gameObject.GetComponent<FreeLookCam>().enabled = true;

        
    }
} 