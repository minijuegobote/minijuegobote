﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(new Vector3(0, 30f, 0) * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
         if (other.tag == "Player")
         {
            Destroy(gameObject);
        }
    }

}
