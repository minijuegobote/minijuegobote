﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircuitosControl : MonoBehaviour {

    List<string> camino = new List<string>() { "Escoje un Circuito ", "Circuito 1", "Circuito 2", "Circuito 3", "Circuito 4", "Circuito 5" };

    public List<GameObject> caminoPrefabs = new List<GameObject>();

    public Dropdown CaminoOpcion;
    public Transform CircuitoPoint;

    public void InstanciarCamino(int i)
    {
        switch (i)
        {
            case 1:
                GameObject.Instantiate(caminoPrefabs[i], CircuitoPoint.position, Quaternion.identity);
                CaminoOpcion.gameObject.SetActive(false);
                Debug.Log(i);
                break;
            case 2:
                GameObject.Instantiate(caminoPrefabs[i], CircuitoPoint.position, Quaternion.identity);
                CaminoOpcion.gameObject.SetActive(false);
                Debug.Log(i);
                break;
            case 3:
                GameObject.Instantiate(caminoPrefabs[i], CircuitoPoint.position, Quaternion.identity);
                CaminoOpcion.gameObject.SetActive(false);
                Debug.Log(i);
                break;
            case 4:
                GameObject.Instantiate(caminoPrefabs[i], CircuitoPoint.position, Quaternion.identity);
                CaminoOpcion.gameObject.SetActive(false);
                Debug.Log(i);
                break;
            case 5:
                GameObject.Instantiate(caminoPrefabs[i], CircuitoPoint.position, Quaternion.identity);
                CaminoOpcion.gameObject.SetActive(false);
                Debug.Log(i);
                break;
            
           
        }
    }


   
    void Start () {
        CaminoOpcion.AddOptions(camino);
    }
	
    
}
