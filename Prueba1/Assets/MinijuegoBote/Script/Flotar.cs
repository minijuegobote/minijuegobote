﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flotar : MonoBehaviour {

    public  float waterLevel = 4f;
    public float floatHeight = 2f;
    public Vector3 buoyancyCentreOffset;
    public float bounceDamp = 0.05f;

    void FixedUpdate()
    {
        var actionPoint = transform.position + transform.TransformDirection(buoyancyCentreOffset);
        var forceFactor = 1f - ((actionPoint.y - waterLevel) / floatHeight);

        if (forceFactor > 0f)
        {
            var uplift = -Physics.gravity * (forceFactor - GetComponent<Rigidbody>().velocity.y * bounceDamp);
            GetComponent<Rigidbody>().AddForceAtPosition(uplift, actionPoint);
        }
    }

}
